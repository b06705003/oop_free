package com.example.sad_poc;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

public class FirstFragment extends Fragment {

    private static final String TAG = "FirstFragment";
    private static final long COUNDOWN_IN_MILLIS = 10000;

    private AppViewModel viewModel;

    private TextView tvQuestion;
    private TextView tvScore;
    private TextView tvQuestionCount;
    private TextView tvCountDown;
    private RadioGroup radioGroup;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private RadioButton rb4;
    private Button btnConfirm;
    private BottomNavigationView navigationView;

    private ColorStateList textColorDefaultRb;
    private ColorStateList textColorDefaultCd;

    private CountDownTimer countDownTimer;
    private long timeLeftInMillis;


    private List<Word> wordList;
    private int questionCounter;
    private int questionCountTotal;
    private Word currentWord;
    private Question currentQuestion;

    private int score;
    private boolean isAnswered;

    private ProgressBar pb;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.first_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // access viewModel
        viewModel = new ViewModelProvider(requireActivity()).get(AppViewModel.class);
        viewModel.initialize();
        viewModel.fillFirstQueue();
        wordList = new ArrayList<>(viewModel.firstQueue);

        questionCountTotal = wordList.size();
        questionCounter = 0;

        // access views
        tvQuestion = view.findViewById(R.id.tv_question);
        tvScore = view.findViewById(R.id.tv_score);
        tvQuestionCount = view.findViewById(R.id.tv_question_count);
        tvCountDown = view.findViewById(R.id.tv_countdown);
        radioGroup = view.findViewById(R.id.radio_group);
        rb1 = view.findViewById(R.id.radio_btn1);
        rb2 = view.findViewById(R.id.radio_btn2);
        rb3 = view.findViewById(R.id.radio_btn3);
        rb4 = view.findViewById(R.id.radio_btn4);
        btnConfirm = view.findViewById(R.id.confirm_btn);
        pb = view.findViewById(R.id.progressBar); // retrieve progress bar
        navigationView = getActivity().findViewById(R.id.bottom_navigation);

        textColorDefaultRb = rb1.getTextColors();
        textColorDefaultCd = tvCountDown.getTextColors();

        showNextQuestion();

        // setup confirm button
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAnswered) {
                    if (rb1.isChecked() || rb2.isChecked() || rb3.isChecked() || rb4.isChecked()) {
                        checkAnswer();
                    } else {
                        Toast.makeText(getContext(), "Please select an answer", Toast.LENGTH_LONG).show();
                    }
                } else { // question is answered
                    showNextQuestion();
                }
            }
        });
    }

    private void showNextQuestion() {

        if (questionCountTotal == 0) { // there are no words left
            Toast.makeText(getContext(), "恭喜，您已背完所有單字!", Toast.LENGTH_LONG).show();
            navigationView.setSelectedItemId(R.id.nav_book);
            return;
        }

        rb1.setTextColor(textColorDefaultRb);
        rb2.setTextColor(textColorDefaultRb);
        rb3.setTextColor(textColorDefaultRb);
        rb4.setTextColor(textColorDefaultRb);
        radioGroup.clearCheck();

        if (questionCounter < questionCountTotal) { // quiz is not finished
            Log.d(TAG, questionCounter + " < " + questionCountTotal);
            currentWord = wordList.get(questionCounter);
            currentQuestion = currentWord.getQuestion1();
            List<String> options = currentQuestion.getOptions();

            // change the color of currentWord
            String questionText = currentQuestion.getQuestion();
            String questionWordColored = "<font color='#FF0000'>" + currentWord.getEnglish() + "</font>";
            questionText = questionText.replace(currentWord.getEnglish(), questionWordColored);

            tvQuestion.setText(Html.fromHtml(questionText));
            rb1.setText(options.get(0));
            rb2.setText(options.get(1));
            rb3.setText(options.get(2));
            rb4.setText(options.get(3));

            questionCounter++;
            tvQuestionCount.setText("Question: " + questionCounter + "/" + questionCountTotal);
            isAnswered = false;
            btnConfirm.setText("Confirm");

            timeLeftInMillis = COUNDOWN_IN_MILLIS;
            startCountDown();
        } else {
            finishQuiz();
        }
    }

    private void startCountDown() { // timer
        countDownTimer = new CountDownTimer(timeLeftInMillis , 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timeLeftInMillis = 0;
                updateCountDownText();
                checkAnswer();
            }
        }.start();
    }

    private void updateCountDownText() { // update ui for timer
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        tvCountDown.setText(timeFormatted);

        if(timeLeftInMillis < 5000) {
            tvCountDown.setTextColor(Color.RED);
        } else {
            tvCountDown.setTextColor(textColorDefaultCd);
        }
    }

    private void checkAnswer() { // check if answer is true
        isAnswered = true;

        countDownTimer.cancel();

        RadioButton rbSelected = getView().findViewById(radioGroup.getCheckedRadioButtonId());
        int selectedIndex = radioGroup.indexOfChild(rbSelected);

        if (selectedIndex == currentQuestion.getAnswer()) {
            score++;
            tvScore.setText("Score: " + score);
        }

        showSolution();
    }

    private void showSolution() { // show the solution of the question
        rb1.setTextColor(Color.parseColor("#E74E4E"));
        rb2.setTextColor(Color.parseColor("#E74E4E"));
        rb3.setTextColor(Color.parseColor("#E74E4E"));
        rb4.setTextColor(Color.parseColor("#E74E4E"));

        switch (currentQuestion.getAnswer()) {
            case 0:
                rb1.setTextColor(Color.parseColor("#4DCF7C"));
                tvQuestion.setText("Option 1 is correct");
                break;
            case 1:
                rb2.setTextColor(Color.parseColor("#4DCF7C"));
                tvQuestion.setText("Option 2 is correct");
                break;
            case 2:
                rb3.setTextColor(Color.parseColor("#4DCF7C"));
                tvQuestion.setText("Option 3 is correct");
                break;
            case 3:
                rb4.setTextColor(Color.parseColor("#4DCF7C"));
                tvQuestion.setText("Option 4 is correct");
                break;
        }
        // update progress bar
        pb.setProgress((int)(questionCounter*100/questionCountTotal));

        if (questionCounter < questionCountTotal) {
            btnConfirm.setText("Next");
        } else {
            btnConfirm.setText("Finish");
        }
    }

    private void finishQuiz() { // finish the quiz
        showFinishDialog();
        viewModel.fillSecondQueue();
    }

    private void showFinishDialog() { // show the finish dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("總共題數: " + questionCountTotal + "\n正確題數: " + score)
                .setTitle("完成一階")
                .setCancelable(false)
                .setPositiveButton("進入二階", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        navigationView.setSelectedItemId(R.id.nav_second);
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onDestroy() {
        super.onDestroy();
        if(countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
