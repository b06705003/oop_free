package com.example.sad_poc;

import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AppViewModel extends ViewModel {

    public static final int WORDS_PER_DAY = 3;

    public Queue<Word> preQueue;
    public Queue<Word> firstQueue;
    public Queue<Word> secondQueue;
    public Queue<Word> thirdQueue;
    public Queue<Word> postQueue;

    // initialize queues
    public boolean initialize() {
        if (preQueue == null) { // not initialized yet
            preQueue = new LinkedList<>(Word.createWordList());
            firstQueue = new LinkedList<>();
            secondQueue = new LinkedList<>();
            thirdQueue = new LinkedList<>();
            postQueue = new LinkedList<>();
            return true;
        }
        return false;
    }

    public void fillFirstQueue() { // add words to firstQueue from preQueue
        int diff = WORDS_PER_DAY - firstQueue.size();
        for (int i = 0; i < diff; i++) {
            if (!preQueue.isEmpty())
                firstQueue.offer(preQueue.poll());
        }
    }

    public void fillSecondQueue() { // add words to secondQueue from firstQueue
        int diff = WORDS_PER_DAY - secondQueue.size();
        for (int i = 0; i < diff; i++) {
            if (!firstQueue.isEmpty())
                secondQueue.offer(firstQueue.poll());
        }
    }

    public void fillThirdQueue() { // add words to thirdQueue from secondQueue
        int diff = WORDS_PER_DAY - thirdQueue.size();
        for (int i = 0; i < diff; i++) {
            if (!secondQueue.isEmpty())
                thirdQueue.offer(secondQueue.poll());
        }
    }

    public void fillPostQueue() { // add words to postQueue from thirdQueue

        while (!thirdQueue.isEmpty()) {
            postQueue.offer(thirdQueue.poll());
        }
    }


}
